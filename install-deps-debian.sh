#!/bin/sh

su="su"

[ -f /usr/bin/sudo ] && su="sudo"

"$su" apt-get install -y --no-install-recommends build-essential debootstrap squashfs-tools