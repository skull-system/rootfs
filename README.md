# rootfs

This repository contains the `rootfs` generator for Skull.

## Installing dependencies on Debian GNU/Linux 11 (bullseye)

To install the dependencies on Debian GNU/Linux 11 (bullseye), run the following command as root in a **Bourne-like** shell with `GNU coreutils` installed after cloning this repository: `chmod +x ./install-deps-debian.sh && ./install-deps-debian.sh`

# Running the build

To run the build, run the following command as root in a **Bourne-like** shell with `GNU coreutils` installed after cloning this repository: `chmod +x ./start.sh && ./start.sh`