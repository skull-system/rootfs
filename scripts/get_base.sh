# Check if dependencies are installed
which debootstrap &>/dev/null || ( echo "debootstrap not installed." && exit 1 )

# Generate basic Debian rootfs
debootstrap testing out http://deb.debian.org/debian/

# Bind mount directories
mount -o bind /run/ out/run
mount -o bind /dev/ out/dev
mkdir -p out/scripts
mount -o bind scripts out/scripts