#!/bin/sh

# Install Phosh and other essential components.
apt-get update
apt-get install -y phosh phoc gnome-control-center gnome-backgrounds

# Create user with UID 1000
useradd -m -u 1000 skull

# Set password for root and skull
echo "skull:1234"    | chpasswd
echo "root:password" | chpasswd

# Enable Phosh
mkdir -p /etc/systemd/system/graphical.target.wants
ln -s /lib/systemd/system/phosh.service /etc/systemd/system/graphical.target.wants/phosh.service

# Install the Linux kernel and generate the initramfs
apt-get update
apt-get install -y linux-image-arm64 initramfs-tools
update-initramfs -c -k all
