# Unmount bind mounted directories
umount out/run
umount out/dev
umount out/scripts

# Generate out.tar.gz
mksquashfs out rootfs.squashfs -b 131072 -comp xz -always-use-fragments

# Echo/print final details
echo ""
echo "           FINISHED           "
echo "=============================="
echo ""
echo "rootfs squashfs : rootfs.squashfs"
echo "rootfs directory: out/"
echo "main passcode   : 1234"
echo "root password   : password"
