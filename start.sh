#!/bin/sh

rm -rf out/ root.squashfs

sh scripts/get_base.sh
chmod 755 out/scripts/edit.sh && \
  chroot out /scripts/edit.sh
sh scripts/gen_rootfs.sh